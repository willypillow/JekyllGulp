JekyllGulp
===
A set of Gulp and shell scripts that I used to use for my blog.

Usage
---
Before using the scripts, fill out the configuration in `gulpconfig.json` and `site.sh`.

`gulp jekyll`
---
Run `jekyll build` in the `$jekyllDir`. ($jekyllDir defined in `gulpconfig.json`)

`gulp optimize-images`
---
Optimize the images in `$jekyllDir/_site`. ($jekyllDir defined in `gulpconfig.json`)

`gulp optimize-css`
---
Optimize the CSS in `$jekyllDir/_site`. ($jekyllDir defined in `gulpconfig.json`)

`gulp optimize-html`
---
Optimize the HTML in `$jekyllDir/_site`. ($jekyllDir defined in `gulpconfig.json`)

`gulp optimize-xml`
---
Optimize the XML in `$jekyllDir/_site`. ($jekyllDir defined in `gulpconfig.json`)

`gulp purge-cache`
---
Purge the Cloudflare cache.

`gulp seo`
---
Send the sitemap in `sitemap` (defined in `gulpconfig.json`) to search engines.

`gulp run`
---
Build the site with all images, CSS, HTML, and XML optimized.

`./site.sh fast-build`
---
Run `jekyll build` in `$jekylldir` (defined at the top of `site.sh`).

`./site.sh build`
---
Build the whole site by running `gulp run`.

`./site.sh fast-serve`
---
Serve the site by running `jekyll serve` in `$jekylldir`. (defined at the top of `site.sh`)

`./site.sh serve`
---
Serve the site built by `gulp run`.

`./site.sh commit [MESSAGE]`
---
Git commit the changes with the message \[MESSAGE\].

`./site.sh push [MESSAGE]`
---
Git commit the changes with the message \[MESSAGE\] and push the repo to `origin`.

`./site.sh site-push`
---
Git push the site in `$jekylldir/_site`. ($jekylldir defined at the top of `site.sh`)

`./site.sh deploy [MESSAGE]`
---
* Git pull the repo.
* Git commit the changes with the message \[MESSAGE\] and push the repo to `origin`.
* Build the whole site by running `gulp run`.
* Git push the site in `$jekylldir/_site`.
* Purge the Cloudflare cache via `gulp purge-cache`
* Send the sitemap to search engines via `gulp seo`

`./site.sh post [TITLE]`
---
Create a post with the title \[TITLE\], automatically filling out most of its front matter.

`./site.sh comment`
---
Update the comments via [JekyllAutoComment](https://gitlab.com/willypillow/JekyllAutoComment).
