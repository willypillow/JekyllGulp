var config = require('./gulpconfig.json');
var	gulp = require('gulp');
var	shell = require('gulp-shell');
var minifyHTML = require('gulp-minify-html');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var uncss = require('gulp-uncss');
var cleanCss = require('gulp-clean-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var jpegtran = require('imagemin-jpegtran');
var gifsicle = require('imagemin-gifsicle');
var replace = require('gulp-replace');
var fs = require('fs');
var flatmap = require('gulp-flatmap');
var cloudflare = require('gulp-cloudflare');
var path = require('path');
var request = require('request');
var prettyData = require('gulp-pretty-data');

var pwd = path.resolve('.');
var jekyllDir = path.resolve(config.jekyllDir);

gulp.task('jekyll', function() {
  process.chdir(jekyllDir);
	return gulp.src('index.html', { read: false })
		.pipe(shell([
			'jekyll build'
		]));
});

gulp.task('optimize-images', function () {
  process.chdir(jekyllDir);
	return gulp.src(['_site/**/*.jpg', '_site/**/*.jpeg', '_site/**/*.gif', '_site/**/*.png'])
		.pipe(imagemin({
			progressive: false,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant(), jpegtran(), gifsicle()]
		}))
		.pipe(gulp.dest('_site/'));
});

gulp.task('optimize-css', function() {
  process.chdir(jekyllDir);
  return gulp.src('_site/css/main.css')
    .pipe(autoprefixer())
    .pipe(uncss({
 	   html: ['_site/**/*.html'],
 	   ignore: []
    }))
    .pipe(cleanCss())
    .pipe(gulp.dest('_site/css/'));
});

gulp.task('optimize-html', function() {
  process.chdir(jekyllDir);
	return gulp.src('_site/**/*.html')
		.pipe(minifyHTML({
			quotes: true
		}))
    .pipe(flatmap(function(stream, file) {
      return gulp.src('_site/css/main.css')
        .pipe(uncss({
          html: [file.path],
          ignore: []
        }))
        .pipe(cleanCss())
        .pipe(flatmap(function(stream2, file2) {
          return stream.pipe(replace(/<link rel=\"stylesheet\" href=\"\/css\/main.css\"[^>]*>/, function(s){
            return '<style>' + file2.contents + '</style>';
          }));
        }));

    }))
    .pipe(gulp.dest('_site/'));
});

gulp.task('optimize-xml', function() {
  process.chdir(jekyllDir);
	return gulp.src('_site/**/*.xml')
		.pipe(prettyData({type: 'minify', preserveComments: true}))
    .pipe(gulp.dest('_site/'));
});

gulp.task('purge-cache', function() {
	var options = {
		token: config.cfToken,
		email: config.cfEmail,
		domain: config.cfDomain
	};
	cloudflare(options);
});

gulp.task('seo', function(cb) {
    request('http://www.google.com/webmasters/tools/ping?sitemap=' + config.sitemap);
    request('http://www.bing.com/webmaster/ping.aspx?siteMap=' + config.sitemap);
    cb();
});

gulp.task('run', function(callback) {
	runSequence(
		'jekyll',
		'optimize-images',
		'optimize-css',
		'optimize-html',
    'optimize-xml',
		callback
	);
});

