#!/bin/bash

jekyllDir="PUT_YOUR_JEKYLL_DIRECTORY_HERE"
jekyll="jekyll"

case "$1" in
  fast-build)
    cd "$jekyllDir"
    $jekyll build
    ;;
  build)
    gulp run
    ;;
  fast-serve)
    cd "$jekyllDir"
    $jekyll serve
    ;;
  serve)
    $0 build
    cd "$jekyllDir"
    $jekyll serve --skip-initial-build
    ;;
  commit)
    git add -A
    git commit -m "$2"
    ;;
  push)
    $0 commit "$2"
    git push origin master
    ;;
  site-push)
    COMMIT=`git log -n 1 --format=%H`
    cd "$jekyllDir/_site"
    git add -A
    git commit -m "Update to $COMMIT"
    git push origin master
    ;;
  deploy)
    git pull
    $0 push "$2"
    $0 build
    $0 site-push
    gulp purge-cache seo
    ;;
  post)
    TITLE=`echo "$2" | sed -e 's/[^[:alnum:]]/-/g' | tr -s '-' | sed -e 's/^-//g' -e 's/-$//g' | tr A-Z a-z`
    SHORTDATE=`date "+%Y-%m-%d"`
    FILENAME="$jekyllDir/_posts/$SHORTDATE-$TITLE.md"
    echo '---' > "$FILENAME"
    echo 'layout: post' >> "$FILENAME"
    echo "title: \"$2\"" >> "$FILENAME"
    date "+date: %Y-%m-%d %T +0800" >> "$FILENAME"
    echo 'moddate: ' >> $FILENAME
    echo 'tags: ' >> "$FILENAME"
    echo '---' >> "$FILENAME"
    echo >> "$FILENAME"
    vim +"normal 6G" +"normal $" "$FILENAME"
    ;;
  comment)
    python3 imap.py
    $0 deploy "Update comments"
    ;;
esac
